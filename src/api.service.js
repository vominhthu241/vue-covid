import axios from 'axios';
var headerDefault = {
   'X-Access-Token': '5cf9dfd5-3449-485e-b5ae-70a60e997864'
};

function getDateAgo(numberOfDay)
{
  var dayAgo = new Date();
  dayAgo.setDate(dayAgo.getDate() - numberOfDay);
  return dayAgo.getFullYear() + '-' + dayAgo.getMonth() + '-' + dayAgo.getDate();
}

var start = getDateAgo(2);
var end = getDateAgo(1);

export default {
    getCountries: () => {
        return axios.get('https://api.covid19api.com/countries', {
                headers: headerDefault
           });
    },
    getCountryResults(countryCode = 'vietnam') {
        return axios.get(`https://api.covid19api.com/country/${countryCode}?from=${start}&to=${end}`, {
            headers: headerDefault
        });
    }
}